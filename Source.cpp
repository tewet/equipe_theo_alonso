#include <iostream>
using namespace std;

void permuter(int & x, int &y);
void permuter2(int *x, int *y);

int main()
{
	// m�thode 1
	int x = 2, y = 7, temp = 0;
	cout << "Before swapping : x = " << x << " , y = " << y << endl << endl;
	temp = y;
	y = x;
	x = temp;
	cout << "After swapping : x = " << x << " , y = " << y << endl << endl;

	// m�thode 2
	cout << "Permutation fonction 1 !\n\n";
	cout << "Before swapping : x = " << x << " , y = " << y << endl << endl;
	permuter(x, y);
	cout << "After swapping : x = " << x << " , y = " << y << endl << endl;

	//m�thode 3
	cout << "Permutation fonction 2 !\n\n";
	cout << "Before swapping : x = " << x << " , y = " << y << endl << endl;
	permuter2(&x, &y);
	cout << "After swapping : x = " << x << " , y = " << y << endl << endl;

	system("pause");
	return 0;
}

void permuter(int & x, int &y) // il faut utiliser les pointeurs d'adresses '&' sinon il va pas trouver la m�me variable.
{
	int temp = 0;
	temp = y;
	y = x;
	x = temp;
}

void permuter2(int *x, int *y) // UNE VARIABLE QUI CONTIENT UNE ADRESSE EST UN POINTEUR !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
{
	int temp = 0;
	temp = *y;
	*y = *x;
	*x = temp;
}